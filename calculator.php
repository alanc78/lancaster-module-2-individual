<!DOCTYPE HTML>
<html>
   <head>
        <meta charset="utf-8">
	<title>Calculator</title>
   </head>
   <body>
	<form action="calculator.php" method="GET">
	    <p>
		<input type="number" name="num1" id="num1"/>
	    </p>
	    <p>
		<input type="number" name="num2" id="num2"/>
	    </p>
	    <p>
		<strong>Operation:</strong>
		<input type="radio" name="operation" value="add" /> <label>Addition</label>
		<input type="radio" name="operation" value="subtract" /> <label>Subtraction</label>
		<input type="radio" name="operation" value="multiply" /> <label>Multiplication</label>
		<input type="radio" name="operation" value="divide" /> <label>Division</label>
	    </p>
	    <p>
		<input type="submit" value="calculate"/>
	    </p>
	</form>
	<?php
	    $num1 = $_GET['num1'];
	    $num2 = $_GET['num2'];
	    $num3;
	    $operation = $_GET['operation'];

	    switch ($operation) {
		case "add":
		   $num3 = $num1 + $num2;
		   echo "$num3";
		   break;
		case "subtract":
		   $num3 = $num1 - $num2;
		   echo "$num3";
		   break;
		case "multiply":
		   $num3 = $num1 * $num2;
		   echo "$num3";
		   break;
		case "divide":
		   if($num2 != 0) {
			$num3 = $num1 / $num2;
			echo "$num3";
		   }
		   else
			echo "Cannot divide by zero!";
		   break;
	    }
	?>
   </body>
</html>

